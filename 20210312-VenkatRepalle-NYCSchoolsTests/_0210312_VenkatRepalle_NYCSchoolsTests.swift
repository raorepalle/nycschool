//
//  _0210312_VenkatRepalle_NYCSchoolsTests.swift
//  20210312-VenkatRepalle-NYCSchoolsTests
//
//  Created by Venkateswara Rao Repalle on 3/15/21.
//

import XCTest
@testable import _0210312_VenkatRepalle_NYCSchools

class _0210312_VenkatRepalle_NYCSchoolsTests: XCTestCase {

    var viewModel = NYCSchoolViewModel()
    override func setUpWithError() throws {
        
        let expectation = self.expectation(description: "schools")
        
        viewModel.makeNatworkCalls {
            expectation.fulfill()
        }
        waitForExpectations(timeout: 25, handler: nil)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        viewModel = NYCSchoolViewModel()
    }

    func testSchoolsCount() throws {
        XCTAssertEqual(viewModel.listSchools.count, 440)
        
    }
    func testSATInfoCount() throws {
        XCTAssertEqual(viewModel.listSchoolSATInfo.count, 478)
    }//01M292
    func testSATInfoData() throws {
        let satInfo =  viewModel.getSchoolForId(schoolId: "21K728")
        XCTAssert(satInfo != nil)
    }
    
    func testSATInfoNotFoundData() throws {
        //Clinton School Writers & Artists we do not have SAT Info
        let satInfo =  viewModel.getSchoolForId(schoolId: "02M260")
        XCTAssert(satInfo == nil)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
