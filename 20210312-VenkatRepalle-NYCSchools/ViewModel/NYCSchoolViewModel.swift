//
//  NYCSchoolViewModel.swift
//  20210312-VenkatRepalle-NYCSchools
//
//  Created by Venkateswara Rao Repalle on 3/15/21.
//

import Foundation


class NYCSchoolViewModel {
    
    private let schoolDataURL : String = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    private let schoolInfoURL : String = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    private var gotSchoolData : Bool = false
    private var gotSchoolInfo : Bool = false
    var listSchoolSATInfo: [NYCSchoolInfo] = []
    var listSchools : [NYCSchool] = []
    //Makes network call for both URLs and wait until we get response from both then sends response to View COntroller. CUrrently not handling Error codes just sending the Empty data
    func makeNatworkCalls(completionHandler:@escaping () -> Void)
    {
        NetworkManager().makeRequest(urlStr: schoolDataURL, reqType: RequestType.SchoolData) { (responseData) in
            print("Got schoolDataURL")
            self.listSchools = responseData.listSchools
            self.gotSchoolData = true
            if(self.gotSchoolInfo)
            {
                
                DispatchQueue.main.async {
                    print("Callsing completion ")
                    completionHandler()
                }
            }
            
        }
        
        NetworkManager().makeRequest(urlStr: schoolInfoURL, reqType: RequestType.SchoolInfo) { (responseData) in
            self.listSchoolSATInfo = responseData.listSchoolSATInfo
            print("Got gotSchoolInfo")
            self.gotSchoolInfo = true
            if(self.gotSchoolData)
            {
                DispatchQueue.main.async {
                    print("Callsing completion ")
                    completionHandler()
                }
                
            }
        }
    }
    //Get the SAT Info object for geven School id. If not avaible return will be sent.
    func getSchoolForId(schoolId : String) -> NYCSchoolInfo? {
        
        for schoolInfo in self.listSchoolSATInfo {
            
            if(schoolInfo.dbn == schoolId)
            {
                return schoolInfo
            }
        }
        
        return nil
    }
    
    
}
