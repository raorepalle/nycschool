//
//  NYCSchool.swift
//  20210312-VenkatRepalle-NYCSchools
//
//  Created by Venkateswara Rao Repalle on 3/15/21.
//

import Foundation

struct NYCSchool : Codable{
    let dbn, school_name,location,phone_number : String
}

