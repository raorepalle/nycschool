//
//  SchoolResponseData.swift
//  20210312-VenkatRepalle-NYCSchools
//
//  Created by Venkateswara Rao Repalle on 3/15/21.
//

import Foundation

struct SchoolResponseData {

    let listSchools : [NYCSchool]
    let listSchoolSATInfo : [NYCSchoolInfo]
    let errorInfo : String
    
    init(listSchoolInfo : [NYCSchoolInfo]) {
       
        self.listSchoolSATInfo = listSchoolInfo
        self.listSchools = []
        self.errorInfo = ""
    }
    init(listSchoolData : [NYCSchool])
    {
       
        self.listSchools = listSchoolData
        self.listSchoolSATInfo = []
        self.errorInfo = ""
    }
    
    init(errorInfo : String)
    {
        self.errorInfo = errorInfo
        self.listSchoolSATInfo = []
        self.listSchools = []
    }
    
    
}
