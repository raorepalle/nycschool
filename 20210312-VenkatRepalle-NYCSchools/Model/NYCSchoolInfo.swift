//
//  NYCSchoolInfo.swift
//  20210312-VenkatRepalle-NYCSchools
//
//  Created by Venkateswara Rao Repalle on 3/15/21.
//

import Foundation

struct NYCSchoolInfo : Codable {
    let dbn, school_name,num_of_sat_test_takers,sat_critical_reading_avg_score,sat_math_avg_score,sat_writing_avg_score : String
}
