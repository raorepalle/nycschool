//
//  SchoolInfoViewController.swift
//  20210312-VenkatRepalle-NYCSchools
//
//  Created by Venkateswara Rao Repalle on 3/15/21.
//

import Foundation
import UIKit

class SchoolInfoViewController: UIViewController {
    var schoolInfo: NYCSchoolInfo? = nil
    
    @IBOutlet weak var lblSchoolName: UILabel!
    @IBOutlet weak var lblNoSATTakers: UILabel!
    @IBOutlet weak var lblSATMathScore: UILabel!
    @IBOutlet weak var lblSATEnglishWriting: UILabel!
    
    @IBOutlet weak var lblSATEnglishReading: UILabel!
    
    //Just displaying SAT information
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let name = schoolInfo?.school_name
        {
            lblSchoolName.text = name
        }
        
        if let satTakers = schoolInfo?.num_of_sat_test_takers
        {
            lblNoSATTakers.text =  "No of SAT Takers                        : \(satTakers)"
        }
        
        if let mathScore = schoolInfo?.sat_math_avg_score
        {
            lblSATMathScore.text =  "SAT Math avg score                    : \(mathScore)"
        }
        
        if let reading = schoolInfo?.sat_critical_reading_avg_score
        {
            lblSATEnglishReading.text =  "SAT Englisg Reading avg score  : \(reading)"
        }
        
        if let writing = schoolInfo?.sat_writing_avg_score
        {
            lblSATEnglishWriting.text =  "SAT Englisg Writing avg score   : \(writing)"
        }
        
        // Do any additional setup after loading the view.
    }


}
