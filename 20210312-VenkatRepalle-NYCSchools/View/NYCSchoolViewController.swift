//
//  NYCSchoolViewController.swift
//  20210312-VenkatRepalle-NYCSchools
//
//  Created by Venkateswara Rao Repalle on 3/15/21.
//

import Foundation
import UIKit
//Disply list of schools
class NYCSchoolViewController: UITableViewController {
    
    private var indicator: UIActivityIndicatorView? = nil
    private let schoolModel = NYCSchoolViewModel()
    private var schoolInfo : NYCSchoolInfo? = nil
    
    //Loading School and address using Table View. I am loading both files at a time and displaying. We can also load first file first and the load the second file while loading the Table view.Given more time I could have handled all failure scenarios.
    override func viewDidLoad() {
        super.viewDidLoad()
        addProgressIndicator()
        self.title = "Schools"
        self.indicator?.startAnimating()
        //Setting navigation color
        self.navigationController?.navigationBar.barTintColor = UIColor.systemBlue
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        //Requesting networkdata through ViewModel
        self.schoolModel.makeNatworkCalls { 
            
            print("got completion ")
            if self.schoolModel.listSchools.count > 0
            {
                print("\(self.schoolModel.listSchools.count) -- \(self.schoolModel.listSchoolSATInfo.count)")
                self.tableView.reloadData()
            }
            else
            {
                //By any chance if there is any issue with loading displaying error messages. We need to retry and display
                self.showAlertDisalog()
            }
            self.indicator?.stopAnimating()
        }
    }
    
    func showAlertDisalog()
    {
        let alert = UIAlertController(title: "Data issue", message: "Could not able to load the data. Please try again.", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        
        self.present(alert, animated: true)
    }
    //adding progressbar  while getting data from network
    private func addProgressIndicator()
    {
        self.indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        self.indicator?.color = .gray
        self.indicator?.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        self.indicator?.center = view.center
        self.view.addSubview(self.indicator!)
        self.view.bringSubviewToFront(self.indicator!)
    }
    
    
    //MARK:- Tableview delegate/data source methods
    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(schoolModel.listSchools.count > 0)
        {
            return schoolModel.listSchools.count
        }
        return 0
    }
    
    //Just using Subtitle tableview cell style and setting no of lines to zero so that all data will be displayed
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SCHOOLCELL", for: indexPath) 
        if( schoolModel.listSchools.count  > 0)
        {
            let schoolData : NYCSchool? = schoolModel.listSchools[indexPath.row]
            if schoolData != nil
            {
                cell.textLabel?.text = schoolData?.school_name
                cell.textLabel?.numberOfLines = 0
                cell.detailTextLabel?.text = schoolData?.location
                cell.detailTextLabel?.numberOfLines = 0
            }
            
        }
        
        return cell
    }
    //For selected School getting SAT info and passing to SchoolInfoViewController to display SAT information
    //FOr SOme of the schools SAT inofmration is not available. When we select that school we are displaying
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.schoolInfo = self.schoolModel.getSchoolForId(schoolId: self.schoolModel.listSchools[indexPath.row].dbn  )
        if(self.schoolInfo != nil)
        {
            self.performSegue(withIdentifier: "SHOWSCHOOLINFO", sender: nil)
        }
        else
        {
            showAlertSchoolInfo(schoolName: self.schoolModel.listSchools[indexPath.row].school_name)
        }
    }
    
    func showAlertSchoolInfo(schoolName : String)
    {
        let alert = UIAlertController(title: "SAT Info not available", message: "School SAT info not avaible for \(schoolName). ", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        
        self.present(alert, animated: true)
    }
    //Passing selected school info to SchoolInfoViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let infoController : SchoolInfoViewController = segue.destination as! SchoolInfoViewController
        infoController.schoolInfo = self.schoolInfo
    }
    
    
}
