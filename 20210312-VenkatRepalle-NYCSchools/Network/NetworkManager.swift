//
//  NetworkManager.swift
//  20210312-VenkatRepalle-NYCSchools
//
//  Created by Venkateswara Rao Repalle on 3/15/21.
//

import Foundation
enum RequestType : Int {
    case SchoolData,
         SchoolInfo
       
}
class NetworkManager {
    // Makes network call and creates object using JSONDecoder and sends to View Model
    func makeRequest(urlStr: String,reqType : RequestType,completionHandler: @escaping (SchoolResponseData) -> Void)
        {
           
            
            let session = URLSession.shared
            let url = URL(string: urlStr)
            var request = URLRequest(url: url!)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request) { (data, response, error) in
                
                let decoder = JSONDecoder()
                
                let resData :SchoolResponseData
                if(error != nil)
                {
                    resData = SchoolResponseData(errorInfo: error!.localizedDescription)
                }
                else if( reqType == .SchoolData)
                {
                    let schoolData: [NYCSchool] = try! decoder.decode([NYCSchool].self, from: data!)
                    resData = SchoolResponseData(listSchoolData: schoolData)
                }
                else
                {
                    let schoolInfo: [NYCSchoolInfo] = try! decoder.decode([NYCSchoolInfo].self, from: data!)
                    resData = SchoolResponseData(listSchoolInfo: schoolInfo)
                }
                completionHandler(resData)
               
                
            }
            task.resume()

            
        }
}
